$(document).ready(function() {

  // JS FOR SCROLLING THE ROW OF THUMBNAILS -->

 $(".arrow-right").bind("click", function (event) {
            event.preventDefault();
            $(".vid-list-container").stop().animate({
                scrollLeft: "+=336"
            }, 750);
        });
        $(".arrow-left").bind("click", function (event) {
            event.preventDefault();
            $(".vid-list-container").stop().animate({
                scrollLeft: "-=336"
            }, 750);
        });

// Gets the video src from the data-src on each button

var $videoSrc;  
$('.video-btn').click(function() {
    $videoSrc = $(this).data( "src" );
});
console.log($videoSrc);

  
  
// when the modal is opened autoplay it  
$('#myModal').on('shown.bs.modal', function (e) {
    
// set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you're gonna get
$("#video").attr('src',$videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0" ); 
});


// stop playing the youtube video when I close the modal
$('#myModal').on('hide.bs.modal', function (e) {
    // a poor man's stop video
    $("#video").attr('src',$videoSrc); 
}) ;
    
$(function(){
  $('.modal').on('hidden.bs.modal', function (e) {
    $iframe = $(this).find("iframe");
    $iframe.attr("src", $iframe.attr("src"));
  });
});

        $('.button').click(function() {
  $("iframe").remove();   
});

$('#modal6').on('hidden.bs.modal', function (e) {
  // do something...
  $('#modal6 iframe').attr("src", $("#modal6 iframe").attr("src"));
});

$('#modal4').on('hidden.bs.modal', function (e) {
  // do something...
  $('#modal4 iframe').attr("src", $("#modal4 iframe").attr("src"));
});
  
  

  jQuery(document).bind('hidden.bs.modal', function() {
  // target iframes with src attribute containing 'youtube'
  // (within the facebox-generated .popup wrapper).
  var vid = jQuery('iframe[src*="youtube"]');
  // if such an element exists
  if ( vid.length > 0 ){
    // get iframe's src attribute and cache it to a variable
    var src = vid.attr('src');
    // empty the iframe's src attribute
    // (this will kill the video playing)
    vid.attr('src', '');
    // and restore the iframe src url, ready to be played
    // again when the lightbox is displayed
    vid.attr('src', src);
  }
});
// document ready  
});


